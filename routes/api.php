<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\v1\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*
 * Authentication routes
 */
Route::post('/login', [AuthController::class,'login']);
Route::post('/register', [AuthController::class,'register']);

Route::group(["middleware"=>["auth:api"]], function(){

    // Auth user profile
    Route::get('/profile', [AuthController::class,'profile']);

    // Logout
    Route::get('/logout',[AuthController::class,'logout']);

    // Task resources
    Route::resource('/tasks',\App\Http\Controllers\api\v1\TaskController::class);

    // Task group resources
    Route::post('/task-groups/toggle-member',[\App\Http\Controllers\api\v1\TaskGroupController::class,'addRemoveUserInTaskGroup']);
    Route::resource('/task-groups',\App\Http\Controllers\api\v1\TaskGroupController::class);

});
