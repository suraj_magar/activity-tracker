<?php

namespace App\Models;

use App\Enums\DB as Table;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory, SoftDeletes;

    /**@var $table */
    protected $table = Table::TASK;


    /** mass assignment */
    protected $fillable = [
        'title',
        'description',
        'priority',
        'position',
        'status',
        'deadline',
        'estimated_hours',
        'spend_hours',
        'user_id',
        'task_group_id',
    ];


    /**
     * Eloquent relation user
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }


    /**
     * Eloquent relation taskGroup
     * @return HasOne
     */
    public function taskGroup(): HasOne
    {
        return $this->hasOne(TaskGroup::class, 'id', 'task_group_id');
    }
}
