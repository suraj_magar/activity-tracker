<?php

namespace App\Models;

use App\Enums\DB as Table;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    /**@var table */
    protected $table = Table::CATEGORIES;

    /** mass assignments */
    protected $fillable = [
        'title',
        'description',
        'user_id'
    ];
}
