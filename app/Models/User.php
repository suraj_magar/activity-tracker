<?php

namespace App\Models;

use App\AT\Accessors\UserAccessor;
use App\Enums\DB as Table;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use UserAccessor, HasApiTokens, HasFactory, Notifiable, HasRoles , SoftDeletes;

    /**@table */
    protected $table = Table::USERS;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Eloquent relations for categories
     * @returns hasMany
     */
    public function categories(){
        return $this->hasMany(Category::class,'user_id');
    }


    /**
     * Eloquent relation with task group
     * @returns belongsToMany
     */
    public function taskGroups(){
        return $this->belongsToMany(TaskGroup::class, Table::TASK_GROUP_USER );
    }


    /**
     * Eloquest relation tasks
     * @returns hasMany
     */
    public function tasks(){
        return $this->hasMany(Tasks::class,'user_id');
    }
}
