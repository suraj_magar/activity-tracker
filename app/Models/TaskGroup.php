<?php

namespace App\Models;

use App\Enums\DB as Table;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskGroup extends Model
{
    use HasFactory, SoftDeletes;

    /**@table */
    protected $table = Table::TASK_GROUPS;

    /** mass assignment */
    protected $fillable = [
        'name',
        'description',
        'thumbnail',
    ];

    /**
     * Eloquent relation tasks
     * @returns hasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'task_group_id');
    }


    /**
     * Eloquent relation users
     * @returns belongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, Table::TASK_GROUP_USER);
    }
}
