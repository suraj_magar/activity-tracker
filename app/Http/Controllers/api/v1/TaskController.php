<?php

namespace App\Http\Controllers\api\v1;

use App\AT\Services\TaskService;
use App\Http\Controllers\api\BaseApiController;
use App\Http\Requests\Task\TaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TaskController extends BaseApiController
{
    /**
     * @var TaskService
     */
    protected TaskService $taskService;

    /**
     * @param TaskService $taskService
     */
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @param Request $request
     * @return JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        try {
            $tasks = $this->taskService->index($request->all(), ['taskGroup', 'user:id,name,email'])->paginate($request->per_page ?: 10);
            return TaskResource::collection($tasks);
        } catch (\Exception $e) {
            return $this->failure($e->getMessage(), 500, $e);
        }
    }

    /**
     * @param TaskRequest $request
     * @return TaskResource|JsonResponse
     */
    public function store(TaskRequest $request)
    {
        try {
            $task = $this->taskService->store($request->formattedData());
            return $this->success("Task created successfully",new TaskResource($task));
        } catch (\Exception $e) {
            return $this->failure($e->getMessage(), 500, $e);
        }
    }


    /**
     * @param Task $task
     * @param TaskRequest $request
     * @return JsonResponse
     */
    public function update(Task $task,TaskRequest $request)
    {
        try {
            $task = $this->taskService->update($task->id,$request->all());
            return $this->success("Task updated successfully",new TaskResource($task));
        } catch (\Exception $e) {
            return $this->failure($e->getMessage(), 500, $e);
        }
    }

    /**
     * @param Task $task
     * @return int
     */
    public function destroy(Task $task){
        return $task->forceDelete();
    }


    /**
     * @param Task $task
     * @return bool|null
     */
    public function delete(Task $task){
        return $task->forceDelete();
    }

}
