<?php

namespace App\Http\Controllers\api\v1;

use App\AT\Services\UserService;
use App\Enums\UserRoles;
use App\Http\Controllers\api\BaseApiController;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Resources\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseApiController
{

    /**
     * @var UserService
     */
    protected UserService $userService;


    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request): JsonResponse
    {
        /* Validate Login Request */
        $this->validate($request, [
            "email" => "required|email",
            "password" => "required"
        ]);

        // Login Credentials
        $credentials = [
            "email" => $request->email,
            "password" => $request->password
        ];

        if (Auth::attempt($credentials)) {
            $user = Auth::user(); // authenticated user
            $token = $user->createToken(env('APP_NAME') ?: 'AT')->accessToken; // generate user token

            return $this->success(__("Login successful"), [
                "token" => $token,
                "user" => new UserResource($user)
            ]);
        }

        return $this->failure(__("Invalid Credentials"), 401);
    }


    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        try {
            $user = $this->userService->store($request->all());
            $user->assignRole(UserRoles::USER);
            return $this->success(__("Registered successfully!"), $user);
        } catch (\Exception $e) {
            return $this->failure($e->getMessage());
        }
    }


    /**
     * @return JsonResponse
     */
    public function profile(): JsonResponse
    {
        try {
            $user = Auth::user();
            return $this->success(__("User profile!"), new UserResource($user));
        } catch (\Exception $e) {
            return $this->failure($e->getMessage());
        }
    }


    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        try {
            if (Auth::check()) {
                Auth::user()->token()->revoke();
                return $this->success("Logout successful!");
            }
        } catch (\Exception $e) {
            return $this->failure($e->getMessage());
        }
    }
}
