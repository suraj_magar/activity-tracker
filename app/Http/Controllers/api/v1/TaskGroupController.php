<?php

namespace App\Http\Controllers\api\v1;

use App\AT\Services\TaskGroupService;
use App\Http\Controllers\api\BaseApiController;
use App\Http\Requests\Task\TaskGroupRequest;
use App\Http\Requests\Task\ToggleUserInTaskGroupRequest;
use App\Http\Resources\TaskGroupResource;
use App\Models\TaskGroup;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TaskGroupController extends BaseApiController
{
    /**
     * @var TaskService
     */
    protected TaskGroupService $taskGroupService;

    /**
     * @param TaskGroupService $taskGroupService
     */
    public function __construct(TaskGroupService $taskGroupService)
    {
        $this->taskGroupService = $taskGroupService;
    }

    /**
     * @param Request $request
     * @return JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        try {
            $taskGroups = $this->taskGroupService->index($request->all(), ['users:id,name,email'])->paginate($request->per_page ?: 10);
            return TaskGroupResource::collection($taskGroups);
        } catch (\Exception $e) {
            return $this->failure($e->getMessage(), 500, $e);
        }
    }

    /**
     * @param TaskGroupRequest $request
     * @return TaskGroupResource|JsonResponse
     */
    public function store(TaskGroupRequest $request)
    {
        try {
            $taskGroup = $this->taskGroupService->store($request->all());
            return $this->success("Task group created successfully", new TaskGroupResource($taskGroup));
        } catch (\Exception $e) {
            return $this->failure($e->getMessage(), 500, $e);
        }
    }


    /**
     * @param TaskGroup $taskGroup
     * @param TaskGroupRequest $request
     * @return JsonResponse
     */
    public function update(TaskGroup $taskGroup, TaskGroupRequest $request)
    {
        try {
            $taskGroup = $this->taskGroupService->update($taskGroup->id, $request->all());
            return $this->success("Task group updated successfully", new TaskGroupResource($taskGroup));
        } catch (\Exception $e) {
            return $this->failure($e->getMessage(), 500, $e);
        }
    }

    /**
     * @param TaskGroup $taskGroup
     * @return int
     */
    public function destroy(TaskGroup $taskGroup)
    {
        return $taskGroup->forceDelete();
    }


    /**
     * @param TaskGroup $taskGroup
     * @return bool|null
     */
    public function delete(TaskGroup $taskGroup)
    {
        return $taskGroup->forceDelete();
    }


    /**
     * @param ToggleUserInTaskGroupRequest $request
     * @return JsonResponse
     */
    public function addRemoveUserInTaskGroup(ToggleUserInTaskGroupRequest $request)
    {
        try {
            $this->taskGroupService->addRemoveUser(
                $request->task_group_id,
                $request->user_id,
                $request->action
            );

            return $this->success(
                $request->action === 'add' ? "User added to task group" : "User removed from task group",
            );
        } catch (\Exception $e) {
            return $this->failure($e->getMessage(), 500, $e);
        }
    }
}
