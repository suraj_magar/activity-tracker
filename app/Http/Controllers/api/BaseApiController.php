<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BaseApiController extends Controller
{

    /*
     * Api Success Response
     * @params  string $message, $data, int $code
     * @returns JsonResponse
     */
    public function success(string $message = "", $data = [], int $code = 200): JsonResponse
    {

        return response()->json([
            "success" => true,
            "message" => $message,
            "data" => $this->isEmpty($data)?'No data available':$data,
        ], $code);
    }


    /*
      * Api Failure Response
      * @params  string $message, $data, int $code
      * @returns JsonResponse
      */
    public function failure(string $message = "", int $code = 500, $data = []): JsonResponse
    {
        return response()->json([
            "success" => false,
            "message" => $message,
            "data" => $this->isEmpty($data)?'No data available':$data,
        ], $code);
    }

    /**
     * @param $value
     * @return bool
     */
    public function isEmpty($value){
        if(is_array($value)){
            return empty(array_filter($value));
        }

        return empty($value);
    }

}
