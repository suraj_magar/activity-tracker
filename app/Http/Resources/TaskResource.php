<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "title" => $this->title,
            "description" => $this->description,
            "priority" => $this->priority,
            "position" => $this->position,
            "status" => $this->status,
            "estimated_hours" => $this->estimated_hours,
            "spend_hours" => $this->spend_hours,
            "task_group_id" => $this->task_group_id,
            "user_id" => $this->user_id,
            "created_at" => $this->created_at,
            "task_group" => new TaskGroupResource($this->whenLoaded("taskGroup")),
            "user" => new UserResource($this->whenLoaded("user"))
        ];
    }
}
