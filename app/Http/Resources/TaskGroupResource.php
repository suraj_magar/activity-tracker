<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "name"=> $this->name,
            "description"  => $this->description,
            "thumbnail" => $this->thumbnail,
            "tasks" => TaskResource::collection($this->whenLoaded("tasks")),
            "users" => UserResource::collection($this->whenLoaded("users")),
            "created_at" => $this->created_at
        ];
    }
}
