<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class ToggleUserInTaskGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|numeric',
            'task_group_id' => 'required|numeric',
            'action' => 'required|in:add,remove'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.numeric' => 'Invalid user id',
            'user_id.required' => 'User id is required',
            'task_group_id.numeric'=> 'Invalid task group id',
            'task_group_id.required'=> 'Task group id is required',
            'action.in'=> 'Action must be either add or remove'
        ];
    }
}
