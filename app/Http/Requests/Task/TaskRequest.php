<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'estimated_hours' => 'nullable|numeric',
            'spend_hours' => 'nullable|numeric',
            'user_id' => 'nullable|numeric',
            'task_group_id' => 'nullable|numeric',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(){
        return [
            'estimated_hours.numeric' => 'Invalid estimated hours',
            'spend_hours.numeric' => 'Invalid spend hours',
            'user_id.numeric' => 'Invalid user',
            'task_group_id.numeric' => 'Invalid task group',
        ];
    }

    /**
     * @return array
     */
    public function formattedData(): array
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'priority' => $this->priority,
            'position' => $this->position,
            'status' => $this->title,
            'deadline' => $this->deadline,
            'estimated_hours' => $this->estimated_hours,
            'spend_hours' => $this->spend_hours,
            'user_id' => $this->user_id,
            'task_group_id' => $this->task_group_id,
        ];
    }
}
