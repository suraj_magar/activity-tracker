<?php

namespace App\AT\Accessors;

trait UserAccessor
{
    public function getRoleAttribute(){
        if($this->roles && count($this->roles)){
            return $this->roles[0]->name;
        }

        return null;
    }
}
