<?php

namespace App\AT\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository{

    /**@var Model */
    protected $repositoryModel =  Category::class;

}
