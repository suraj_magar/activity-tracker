<?php

namespace App\AT\Repositories;

use App\Models\TaskGroup;

class TaskGroupRepository extends BaseRepository{

    /**@var Model */
    protected $repositoryModel =  TaskGroup::class;

}
