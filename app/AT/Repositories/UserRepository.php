<?php
namespace App\AT\Repositories;

use App\Models\User;

class UserRepository extends  BaseRepository {

    /**
     * @var string
     */
    protected $repositoryModel = User::class;
}
