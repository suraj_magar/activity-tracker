<?php

namespace App\AT\Repositories;

use App\Models\Task;

class TaskRepository extends BaseRepository{

    /**@var Model */
    protected $repositoryModel =  Task::class;

}
