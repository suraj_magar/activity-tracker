<?php

namespace App\AT\Repositories;

use Prettus\Repository\Eloquent\BaseRepository as PrettusRepository;

class BaseRepository  extends PrettusRepository{

    /**@var  */
    protected $repositoryModel;
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return $this->repositoryModel;
    }
}
