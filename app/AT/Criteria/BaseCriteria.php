<?php

namespace App\AT\Criteria;

use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Boolean;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

use function PHPUnit\Framework\isEmpty;

abstract class BaseCriteria implements CriteriaInterface
{

    /**
     * @var Builder
     */
    protected $builder;


    /**
     * @var Request|array
     */
    protected $request;


    /**
     * constructor
     * @params Array $request
     */
    public function __construct($request = [])
    {
        $this->request = collect($request);
    }


    /**
     * Apply criteria
     * @params Model $model, RepositoryInterface $repository
     */
    public function apply($model, RepositoryInterface $repository)
    {

        /** set up builder instance */
        $this->builder = $model;

        // call to default function
        if (method_exists($this, 'bootstrap')) {
            call_user_func([$this, 'bootstrap']);
        }

        // filters
        foreach ($this->filters() as $filter => $value) {
            if ($this->isFilterApplicable($filter)) {
                $this->builder = call_user_func_array([$this, $this->getMethodName($filter)], [$value]);
            }
        }

        return $this->builder;
    }


    /**
     * Filters
     * @return Array
     */
    public function filters()
    {
        return $this->request->all();
    }


    /**
     * Check if the function is executable
     * @params String $filter
     */
    public function isFilterApplicable($filter)
    {
        return $this->hasMethod($filter) && !isEmpty($this->request->get($filter));
    }


    /**
     * check if the class has the given method exists
     * @params String $filter
     */
    public function hasMethod($filter)
    {
        return method_exists($this, $this->getMethodName($filter));
    }


    /**
     * check if is empty
     */
    public function isEmpty($value)
    {
        if (is_array($value)) {
            return empty(array_filter($value));
        }

        return empty($value);
    }


    /**
     * Get method name (camel case)
     * @params String
     */
    public function getMethodName($string)
    {
        return Str::camel($string);
    }
}
