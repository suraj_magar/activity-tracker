<?php

namespace App\AT\Criteria;

use App\Enums\Sort;

class TaskCriteria extends BaseCriteria {

    /**
     * @param $user_id
     * @return mixed
     */
    public function userId($user_id){
        return $this->builder->where('user_id',$user_id);
    }


    /**
     * @param $task_group_id
     * @return mixed
     */
    public function taskGroupId($task_group_id){
        return $this->builder->where('task_group_id',$task_group_id);
    }


    /**
     * @param $priority
     * @return Builder
     */
    public function priority($priority){
        if(in_array($priority,Sort::asArray()) ){
            return $this->builder->orderBy('priority', $priority);
        }

        return $this->builder;
    }


    /**
     * @param $deadline
     * @return Builder
     */
    public function deadline($deadline){
        if(in_array($deadline,Sort::asArray()) ){
            return $this->builder->orderBy('deadline', $deadline);
        }

        return $this->builder;
    }

    /**
     * @param $trashed
     * @return Builder
     */
    public function trashed($trashed){
        if($trashed){
            return $this->builder->onlyTrashed();
        }
        return $this->builder;
    }

}
