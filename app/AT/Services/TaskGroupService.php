<?php

namespace App\AT\Services;

use App\AT\Repositories\TaskGroupRepository;
use App\Models\TaskGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TaskGroupService
{
    /**
     * @var TaskGroupRepository
     */
    protected TaskGroupRepository $taskGroupRepository;

    /**
     * @param TaskGroupRepository $taskGroupRepository
     */
    public function __construct(TaskGroupRepository $taskGroupRepository)
    {
        $this->taskGroupRepository = $taskGroupRepository;
    }

    /**
     * @param $request
     * @param $with
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index($request = [], $with = [])
    {
        return $this->taskGroupRepository->with($with);
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(array $data)
    {
        DB::beginTransaction();
        try {
            $taskGroup = $this->taskGroupRepository->create($data);
            $this->taskGroupRepository->sync($taskGroup->id, 'users', Auth::id());
            DB::commit();
            return $taskGroup;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param Int $id
     * @param $data
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(int $id, $data)
    {
        DB::beginTransaction();
        try {
            $taskGroup = $this->taskGroupRepository->update($data, $id);
            DB::commit();
            return $taskGroup;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param Int $id
     * @return int
     */
    public function delete(int $id)
    {
        return $this->taskGroupRepository->delete($id);
    }

    /**
     * @param int $task_group_id
     * @param int $user_id
     * @param $add
     * @return void
     */
    public function addRemoveUser(int $task_group_id, int $user_id, $action)
    {
        if ($action === 'add') {
            return $this->taskGroupRepository->sync($task_group_id, 'users', $user_id);
        }else{
            $taskgroup = TaskGroup::findOrFail($task_group_id);
            return $taskgroup->users()->detach($user_id);
        }
    }
}
