<?php

namespace App\AT\Services;

use App\AT\Criteria\TaskCriteria;
use App\AT\Repositories\TaskRepository;
use Illuminate\Support\Facades\DB;

class TaskService extends BaseService
{
    /**
     * @var TaskRepository
     */
    protected TaskRepository $taskRepository;

    /**
     * @param TaskRepository $taskRepository
     */
    public function __construct(TaskRepository $taskRepository){
        $this->taskRepository =$taskRepository;
    }

    /**
     * @param $request
     * @param $with
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index($request = [], $with = []){
        return $this->taskRepository->with($with)->pushCriteria(new TaskCriteria($request));
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Array $data){
        DB::beginTransaction();
        try{
            $task = $this->taskRepository->create($data);
            DB::commit();
            return $task;
        }catch(\Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param Int $id
     * @param $data
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(Int $id, $data){
        DB::beginTransaction();
        try{
            $task = $this->taskRepository->update($data,$id);
            DB::commit();
            return $task;
        }catch(\Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param Int $id
     * @return int
     */
    public function delete(Int $id){
        return $this->taskRepository->delete($id);
    }
}
