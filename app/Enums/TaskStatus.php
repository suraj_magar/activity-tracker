<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class TaskStatus extends Enum
{
    const DONE = 'done';
    const DOING =  'doing';
    const CLOSED = 'closed';
    const REJECTED = 'rejected';
    const ONHOLD = 'onhold';
}
