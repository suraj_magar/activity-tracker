<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class TaskPriority extends Enum
{
    const HIGH = 'high';
    const MEDIUM = 'medium';
    const LOW = 'low';
}
