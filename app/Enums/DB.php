<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class DB extends Enum
{
    const USERS = 'users';
    const TASK_GROUPS = 'task_groups';
    const TASK = 'tasks';
    const CATEGORIES = 'categories';
    const TASK_GROUP_USER = 'task_group_user ';
}
