<?php

use App\Enums\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DB::TASK, function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('description')->nullable();
            $table->string('priority')->nullable();
            $table->bigInteger('position')->nullable();
            $table->string('status')->nullable();
            $table->date('deadline')->nullable();
            $table->float('estimated_hours')->nullable();
            $table->float('spend_hours')->nullable();
            $table->foreignId('user_id')->nullable()->references('id')->on(DB::USERS)->onDelete(null);
            $table->foreignId('task_group_id')->nullable()->references('id')->on(DB::TASK_GROUPS)->onDelete(null);;
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
