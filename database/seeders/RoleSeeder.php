<?php

namespace Database\Seeders;

use App\Enums\UserRoles;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = UserRoles::asArray();

        collect($roles)->each(function ($value, $key) {
            Role::create([
                'name' => $value,
                'guard_name' => 'web'
            ]);
        });
    }
}
